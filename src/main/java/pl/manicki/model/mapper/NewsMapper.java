package pl.manicki.model.mapper;

import org.codehaus.jackson.map.ObjectMapper;
import pl.manicki.model.entity.NewsResponse;

import java.io.IOException;

public class NewsMapper {

    public static NewsResponse mapResponseBodyToNewsResponse(final String responseBody) throws IOException {
        return new ObjectMapper().readValue(responseBody, NewsResponse.class);
    }
}
