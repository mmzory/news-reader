package pl.manicki.model.enums;

public enum ResponseTypeEnum {
    BODY,
    HEADER,
    STATUS,
    VERSION
}
