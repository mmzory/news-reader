package pl.manicki.model.enums;

public enum FileContentSelectorEnum {
    AUTHOR,
    CONTENT,
    DESCRIPTION,
    PUBLISHED_AT,
    SOURCE,
    TITLE,
    URL,
    URL_TO_IMAGE
}
