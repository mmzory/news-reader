package pl.manicki.service;

import pl.manicki.model.enums.ResponseTypeEnum;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class NewsServiceImpl implements NewsService {

    public String readDataFromUri(final String uri, final ResponseTypeEnum responseTypeEnum) throws IOException, InterruptedException {
        final HttpClient httpClient = HttpClient.newBuilder().build();

        final HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(uri))
                .build();

        final HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());

        switch (responseTypeEnum) {
            case HEADER -> {
                return response.headers().toString();
            }
            case STATUS -> {
                return String.valueOf(response.statusCode());
            }
            case VERSION -> {
                return response.version().toString();
            }
            default -> {
                return response.body();
            }
        }
    }
}
