package pl.manicki.service;

import pl.manicki.model.enums.ResponseTypeEnum;

import java.io.IOException;

public interface NewsService {

    String readDataFromUri(final String uri, final ResponseTypeEnum responseTypeEnum) throws IOException, InterruptedException;

}
