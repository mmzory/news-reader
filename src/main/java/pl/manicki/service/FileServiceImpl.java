package pl.manicki.service;

import java.io.FileWriter;
import java.io.IOException;

public class FileServiceImpl implements FileService {

    public void write(final String filePath, final String fileContent) throws IOException {
        FileWriter fileWriter = new FileWriter(filePath);

        fileWriter.write(fileContent);

        fileWriter.flush();
        fileWriter.close();
    }
}
