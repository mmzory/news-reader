package pl.manicki.service;

import java.io.IOException;

public interface FileService {

    void write(final String filePath, final String fileContent) throws IOException;

}
