package pl.manicki;

import pl.manicki.configuration.ApiConfiguration;
import pl.manicki.model.enums.FileContentSelectorEnum;
import pl.manicki.model.mapper.NewsMapper;
import pl.manicki.service.FileServiceImpl;
import pl.manicki.model.entity.NewsResponse;
import pl.manicki.service.NewsServiceImpl;
import pl.manicki.model.enums.ResponseTypeEnum;
import pl.manicki.utils.ContentHelper;

import java.io.IOException;

public class Main {
    private static final String COUNTRY = "pl";
    private static final String CATEGORY = "business";
    private static final String FILE_PATH = "src/main/resources/news.txt";
    private static final String FORMATTED_URL = "https://newsapi.org/v2/top-headlines?country=%s&category=%s&apiKey=%s";

    private static final String API_KEY = ApiConfiguration.API_KEY;

    public static void main(String[] args) {
        try {
            String responseBody = new NewsServiceImpl().readDataFromUri(
                    String.format(FORMATTED_URL, COUNTRY, CATEGORY, API_KEY),
                    ResponseTypeEnum.BODY);

            final NewsResponse newsResponse = NewsMapper.mapResponseBodyToNewsResponse(responseBody);
            final String fileContent = new ContentHelper().prepareData(newsResponse.getArticles(),
                    FileContentSelectorEnum.TITLE,
                    FileContentSelectorEnum.DESCRIPTION,
                    FileContentSelectorEnum.AUTHOR);
            new FileServiceImpl().write(FILE_PATH, fileContent);
        } catch (IOException | InterruptedException ex) {
            ex.printStackTrace();
        }
    }
}
