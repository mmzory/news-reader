package pl.manicki.utils;

import pl.manicki.model.entity.News;
import pl.manicki.model.enums.FileContentSelectorEnum;

import java.util.Arrays;
import java.util.List;

public class ContentHelper {

    private final String CONTENT_SEPARATOR = ":";

    public String prepareData(final List<News> newsList,
                              final FileContentSelectorEnum... fileContentSelectors) {
        final List<FileContentSelectorEnum> fileContentSelectorList = Arrays.stream(fileContentSelectors).toList();
        final StringBuilder fileContent = new StringBuilder();

        newsList.forEach(news -> {
            final StringBuilder fileContentPart = new StringBuilder();
            fileContentSelectorList.forEach(
                    selector -> {
                        addContentSeparatorIfNeeded(fileContentPart);
                        addContentBySelector(news, fileContentPart, selector);
                    });
            addNewLineSeparator(fileContentPart);

            fileContent.append(fileContentPart);
        });

        return new String(fileContent);
    }

    private void addContentBySelector(final News news, final StringBuilder fileContent, final FileContentSelectorEnum selector) {
        switch (selector) {
            case AUTHOR -> fileContent.append(news.getAuthor());
            case CONTENT -> fileContent.append(news.getContent());
            case DESCRIPTION -> fileContent.append(news.getDescription());
            case PUBLISHED_AT -> fileContent.append(news.getPublishedAt());
            case SOURCE -> fileContent.append(news.getSource());
            case TITLE -> fileContent.append(news.getTitle());
            case URL -> fileContent.append(news.getUrl());
            case URL_TO_IMAGE -> fileContent.append(news.getUrlToImage());
        }
    }

    private void addContentSeparatorIfNeeded(final StringBuilder fileContentPart) {
        if (fileContentPart.length() > 0) {
            fileContentPart.append(CONTENT_SEPARATOR);
        }
    }

    private void addNewLineSeparator(final StringBuilder fileContent) {
        fileContent.append(System.lineSeparator());
    }
}
