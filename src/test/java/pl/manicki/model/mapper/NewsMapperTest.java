package pl.manicki.model.mapper;

import org.junit.jupiter.api.Test;
import pl.manicki.model.entity.NewsResponse;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class NewsMapperTest {

    @Test
    void shouldCorrectlyMapJsonValueToNewsResponseObject() throws IOException {
        final String response = "{\"status\":\"ok\",\"totalResults\":1,\"articles\":[{\"source\":{\"id\":0,\"name\":\"sampleName\"},\"author\":\"sampleAuthor\",\"title\":\"sampleTitle\",\"description\":\"sampleDescription\",\"url\":\"sampleUrl\",\"urlToImage\":\"sampleUrlToImage\",\"content\":\"sampleContent\"}]}";

        final NewsResponse newsResponse = NewsMapper.mapResponseBodyToNewsResponse(response);

        assertEquals("ok", newsResponse.getStatus());
        assertEquals(1, newsResponse.getTotalResults());
        assertTrue(newsResponse.getArticles().size() > 0);
        assertEquals("sampleName", newsResponse.getArticles().get(0).getSource().getName());
        assertEquals("sampleAuthor", newsResponse.getArticles().get(0).getAuthor());
    }
}
