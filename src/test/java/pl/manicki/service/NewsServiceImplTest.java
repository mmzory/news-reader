package pl.manicki.service;

import org.junit.jupiter.api.Test;
import pl.manicki.configuration.ApiConfiguration;
import pl.manicki.model.enums.ResponseTypeEnum;

import java.io.IOException;
import java.net.ConnectException;

import static org.junit.jupiter.api.Assertions.*;

class NewsServiceImplTest {

    private static final String COUNTRY = "pl";
    private static final String CATEGORY = "business";
    private static final String FORMATTED_URL = "https://newsapi.org/v2/top-headlines?country=%s&category=%s&apiKey=%s";
    private static final String INCORRECT_URL = "https://incorrect.url";

    @Test
    void shouldCorrectlyGetDataFromSelectedUri() throws IOException, InterruptedException {
        String response = new NewsServiceImpl()
                .readDataFromUri(String.format(FORMATTED_URL, COUNTRY, CATEGORY, ApiConfiguration.API_KEY),
                        ResponseTypeEnum.BODY);

        assertFalse(response.isEmpty());
    }

    @Test
    void shouldThrowAnExceptionIfUriIsIncorrect() {
        assertThrows(ConnectException.class,
                () -> new NewsServiceImpl().readDataFromUri(INCORRECT_URL, ResponseTypeEnum.HEADER));
    }
}
