package pl.manicki.service;

import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileNotFoundException;

import static org.junit.jupiter.api.Assertions.*;

class FileServiceImplTest {

    @Test
    void shouldCorrectlySaveFileWithContent() {
        final String filePath = "./src/test/resources/sampleTestFile.txt";
        final String fileContent = "Sample file content";
        final FileService fileService = new FileServiceImpl();

        assertDoesNotThrow(() -> fileService.write(filePath, fileContent));
        assertTrue(new File(filePath).exists());
    }

    @Test
    void shouldThrowAnExceptionIfFilePathDoesNotExists() {
        final String filePath = "./not/exists/path/sampleTestFile.txt";
        final String fileContent = "Sample file content";
        final FileService fileService = new FileServiceImpl();

        assertThrows(FileNotFoundException.class, () -> fileService.write(filePath, fileContent));
    }
}
