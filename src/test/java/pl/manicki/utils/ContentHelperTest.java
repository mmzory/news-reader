package pl.manicki.utils;

import org.junit.jupiter.api.Test;
import pl.manicki.model.entity.News;
import pl.manicki.model.entity.Source;
import pl.manicki.model.enums.FileContentSelectorEnum;

import java.util.LinkedList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;

class ContentHelperTest {

    @Test
    void shouldCorrectlyReadSpecificDataFromJson() {
        List<String> descriptionList = List.of("description", "description 2", "description 3");
        List<News> newsList = new LinkedList<>();
        newsList.add(new News(new Source(), "author", "title", descriptionList.get(0), "url", "url image", "published at...", "content"));
        newsList.add(new News(new Source(), "author 2", "title 2", descriptionList.get(1), "url 2", "url image 2", "published at... 2", "content 2"));
        newsList.add(new News(new Source(), "author 3", "title 3", descriptionList.get(2), "url 3", "url image 3", "published at... 3", "content 3"));

        String preparedContent = new ContentHelper().prepareData(newsList, FileContentSelectorEnum.DESCRIPTION);

        assertTrue(preparedContent.contains(descriptionList.get(0)));
        assertTrue(preparedContent.contains(descriptionList.get(1)));
        assertTrue(preparedContent.contains(descriptionList.get(2)));
    }
}
